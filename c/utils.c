#include <math.h>
#include <complex.h>
typedef double complex double_complex;


double dot3(const double *x, const double *y){
	int i;
	double d3 = 0.0;
	double xval;
	double yval;

	for(i = 0; i < 3; i++){
	    xval = *(x + i);
		yval = *(y + i);
		d3 += xval * yval;
	}

	return d3;
}


void cross(const double *x,const double *y, double *out){
    out[0] = x[1] * y[2] - x[2] * y[1];
    out[1] = x[2] * y[0] - x[0] * y[2];
    out[2] = x[0] * y[1] - x[1] * y[0];
}


void calc_diff(const double *x, const double *y, double *out){
    int i;
    for(i = 0; i < 3; i++){
        out[i] = x[i] - y[i];
    }
}


void mv3(const double *m, const double *v, double *out){
    int i;
    int j;

    for(i = 0; i < 3; i++){
        out[i] = 0.0;
        for(j = 0; j < 3; j++){
            out[i] += *(m + 3 * i + j) * v[j];
        }
    }

}


// TODO: check what it is..
void vm3_wrong(const double *m, const double *v, double *out){
    int i;
    int j;

    for(i = 0; i < 3; i++){
        out[i] = 0.0;
        for(j = 0; j < 3; j++){
            out[i] += *(m + i + 3 * j) * v[i];
        }
    }

}


void vm3(const double *m, const double *v, double *out){
    int i;
    int j;

    for(i = 0; i < 3; i++){
        out[i] = 0.0;
        for(j = 0; j < 3; j++){
            out[i] += *(m + 3 * j + i) * v[j];
        }
    }

}


void get_offset_positions(const double *x, double *offset,
                          double *cell, double *out){
    double vm[3] = {0.0, 0.0, 0.0};
    int i;
    vm3(cell, offset, vm);
    for(i = 0; i < 3; i++){
        out[i] = x[i] + vm[i];
    }
}

void normalize(double *x){
    int i;
    double norm;
    norm = dot3(x, x);
    norm = sqrt(norm);

    for(i = 0; i < 3; i++){
        x[i] /= norm;
    }
}


int get_incriment(int k){
    if(k == (int) sizeof(int))
        return 1;
    else
        return 2;
}


void complex_mm_nc(const double_complex *x, const double_complex *y, double_complex *out) {

    /* dot(X, Y.T.conj) */

    int i;
    int j;
    int k;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            out[3 * i + j] = 0.0;
            for(k = 0; k < 3; k++){
                out[3 * i + j] += x[3 * i + k] * conj(y[3 * j + k]);
            }
        }
    }
}


void complex_mm_cn(const double_complex *x, const double_complex *y, double_complex *out) {

    /* dot(X.T.conj, Y) */

    int i;
    int j;
    int k;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            out[3 * i + j] = 0.0;
            for(k = 0; k < 3; k++){
                out[3 * i + j] += conj(x[3 * k + i]) * y[3 * k + j];
            }
        }
    }
}


void complex_mm_nn(const double_complex *x, const double_complex *y, double_complex *out) {

    /* dot(X, Y) */

    int i;
    int j;
    int k;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            out[3 * i + j] = 0.0;
            for(k = 0; k < 3; k++){
                out[3 * i + j] += x[3 * i + k] * y[3 * k + j];
            }
        }
    }
}


void mm_nt(const double *x, const double *y, double *out) {

    /* dot(X, Y.T.conj) */

    int i;
    int j;
    int k;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            out[3 * i + j] = 0.0;
            for(k = 0; k < 3; k++){
                out[3 * i + j] += x[3 * i + k] * y[3 * j + k];
            }
        }
    }
}


void complex_to_real_mm_nc(const double_complex *x, const double_complex *y, double *out) {

    /* real(dot(X, Y.T.conj)) */

    int i;
    int j;
    int k;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            out[3 * i + j] = 0.0;
            for(k = 0; k < 3; k++){
                out[3 * i + j] += creal(x[3 * i + k] * conj(y[3 * j + k]));
            }
        }
    }
}


void hadamard_prod_mv_c(const double_complex *x, const double_complex *y, double_complex *out){

    /* matrix vector hadamard product */

    int i;
    int j;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            out[3 * i + j] = x[3 * i + j] * y[j];
        }
    }

}


void hadamard_prod_mm_comp(const double_complex *x, const double_complex *y, double_complex *out){

    /* matrix matrix hadamard product */

    int i;
    int j;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            out[3 * i + j] = x[3 * i + j] * y[3 * i + j];
        }
    }

}



