// #define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include <complex.h>

typedef double complex double_complex;
#define DOUBLEP(a) ((double*)PyArray_DATA(a))
#define INTP(a) ((int*)PyArray_DATA(a))
#define COMPLEXP(a) ((double_complex*)PyArray_DATA(a))
#define CONSTDOUBLEP(a) ((const double*)PyArray_DATA(a))

int get_incriment(int k);
void calc_diff(double *x, double *y, double *out);
void get_offset_positions(double *x, double *offset,
                          double *cell, double *out);
double dot3(const double *x, const double *y);

static PyObject* fillham(PyObject* self, PyObject* args){

    PyArrayObject *ham_obj;
    PyArrayObject *spins_obj;
    PyArrayObject *nmthphi_obj;
    PyArrayObject *nbl1;
    PyArrayObject *nbl2;
    PyArrayObject *e_zdir;
    PyArrayObject *e_k1dir;
    PyArrayObject *e_k2dir;
    PyArrayObject *constr_obj;
    PyArrayObject *nndist;
    PyArrayObject *positions;
    PyArrayObject *offsets;
    PyArrayObject *cell;

    double E0;
    double U;
    PyArrayObject *V;
    double Z;
    double K1;
    double K2;

    if (!PyArg_ParseTuple(args, "DDODODODOOOOOOOOOOO",
     &E0, &U, &V, &Z, &e_zdir,
     &K1, &e_k1dir, &K2, &e_k2dir,
     &ham_obj, &constr_obj, &spins_obj,
     &nmthphi_obj, &nbl1, &nbl2,
     &nndist, &positions, &offsets, &cell
     ))
        return NULL;

    complex double* pham = COMPLEXP(ham_obj);
    double* pspin = DOUBLEP(spins_obj);
    double* p_nmthphi = DOUBLEP(nmthphi_obj);
    double* pe_zdir = DOUBLEP(e_zdir);
    double* pe_k1dir = DOUBLEP(e_k1dir);
    double* pe_k2dir = DOUBLEP(e_k2dir);
    double* p_constr = DOUBLEP(constr_obj);

    int itemsize_nbl1 = PyArray_ITEMSIZE(nbl1);
    int incr = get_incriment(itemsize_nbl1);
    int* n1 = INTP(nbl1);
    int* n2 = INTP(nbl2);
    int k;

    int l;
    int me=0;
    double* poffs2 = DOUBLEP(offsets);
    double* pcell = DOUBLEP(cell);
    double r_ij[3] = {0.0, 0.0, 0.0};
    double offset_pos[3] = {0.0, 0.0, 0.0};
    double r_dist = 0.0;
    int nshells = PyArray_DIMS(V)[0];
    double* pV = DOUBLEP(V);
    double* ppos = DOUBLEP(positions);
    double* pnndist = DOUBLEP(nndist);

    int i1;
    int i2;
    int noa = PyArray_DIMS(nmthphi_obj)[1];
    int listlength = PyArray_DIMS(nbl1)[0];

    double N = 0.0;
    double M = 0.0;
    double th = 0.0;
    double phi = 0.0;
    double e_x = 0.0;
    double e_y = 0.0;
    double e_z = 0.0;

    double e_thx = 0.0;
    double e_thy = 0.0;
    double e_thz = 0.0;

    double e_phx = 0.0;
    double e_phy = 0.0;
    double e_phz = 0.0;

    double H_c_x = 0.0;
    double H_c_y = 0.0;
    double H_c_z = 0.0;

    double cos_th, sin_th, cos_phi, sin_phi;

    // the same spin hoppings
    for(k = 0; k < listlength; k++){
        i1 = n1[k * incr];
        i2 = n2[k * incr];

        get_offset_positions(ppos + 3 * i2, poffs2 + 3 * k,
                     pcell, offset_pos);
        calc_diff(ppos + 3 * i1, offset_pos, r_ij);

        r_dist = sqrt(dot3(r_ij, r_ij));

        for (l = 0; l < nshells; l++){
            if (fabs(pnndist[l] - r_dist) < 0.01){
                me = l;
                break;
            }
        }

        Py_INCREF(ham_obj);
        *(pham + 2*noa * i1 + i2) = pV[me];
        Py_DECREF(ham_obj);

        Py_INCREF(ham_obj);
        *(pham + 2*noa * (i1 + noa) + (i2 + noa)) = pV[me];
        Py_DECREF(ham_obj);

        Py_INCREF(ham_obj);
        *(pham + 2*noa * i2 + i1) = pV[me];
        Py_DECREF(ham_obj);

        Py_INCREF(ham_obj);
        *(pham + 2*noa * (i2 + noa) + (i1 + noa)) = pV[me];
        Py_DECREF(ham_obj);
    }
    for(k = 0; k < noa; k++){
        N = *(p_nmthphi + k);
        M = *(p_nmthphi + k + noa);
        e_x = *(pspin + k * 3 );
        e_y = *(pspin + k * 3 + 1);
        e_z = *(pspin + k * 3 + 2);

        // constr
        if ((1.0 - fabs(e_z)) > 1.0e-8){
            cos_th = e_z;
            sin_th = sqrt(1.0 - e_z*e_z);
            cos_phi = e_x / sin_th;
            sin_phi = e_y / sin_th;
        }
        else{
            cos_th = 1.0;
            sin_th = 0.0;
            cos_phi = 1.0;
            sin_phi = 0.0;
        }

        e_thx = cos_th * cos_phi;
        e_thy = cos_th * sin_phi;
        e_thz = -sin_th;

        e_phx = -sin_phi;
        e_phy = cos_phi;
        e_phz = 0.0;

        H_c_x = *(p_constr + k) * e_thx + *(p_constr + k + noa) * e_phx;
        H_c_y = *(p_constr + k) * e_thy + *(p_constr + k + noa) * e_phy;
        H_c_z = *(p_constr + k) * e_thz + *(p_constr + k + noa) * e_phz;

        // the different spin hoppings
        Py_INCREF(ham_obj);
        *(pham + 2*noa * k + k + noa) = -0.5 * U * M * (e_x + I * e_y)
         - (pe_zdir[0] + I * pe_zdir[1])* Z - (H_c_x + I * H_c_y);
        Py_DECREF(ham_obj);

        Py_INCREF(ham_obj);
        *(pham + 2*noa * (k + noa) + k ) = -0.5 * U * M * (e_x - I * e_y)
        - (pe_zdir[0] - I * pe_zdir[1]) * Z - (H_c_x - I * H_c_y);
        Py_DECREF(ham_obj);
        // diagonal elements

        Py_INCREF(ham_obj);
        *(pham + 2*noa * k + k) = E0 + 0.5 * U * (N + M * e_z) + pe_zdir[2] * Z + H_c_z;
        Py_DECREF(ham_obj);

        Py_INCREF(ham_obj);
        *(pham + 2*noa * (k + noa) + k + noa) = E0 + 0.5 * U * (N - M * e_z) - pe_zdir[2] * Z - H_c_z;
        Py_DECREF(ham_obj);
    }

    Py_INCREF(Py_None);
    return Py_None;
}


static PyMethodDef NCAAMethods[] =
{
     {"fillham", fillham, METH_VARARGS, "hamiltonian"},
     {NULL, NULL, 0, NULL}
};


static struct PyModuleDef cModPyDem =
{
    PyModuleDef_HEAD_INIT,
    "ncaa_module", "Some documentation",
    -1,
    NCAAMethods
};

PyMODINIT_FUNC
PyInit_ncaa_module(void)
{
    import_array();
    return PyModule_Create(&cModPyDem);
}
