import numpy as np


class SteepestDescent(object):
    """
    Steepest descent algorithm
    """

    def __init__(self):
        """
        """
        self.iters = 0

    def __str__(self):
        return 'Steepest Descent algorithm'

    def update_data(self, x_k1, g_k1):

        p_k = -g_k1
        self.iters += 1
        return p_k


class FRcg(SteepestDescent):
    """
    The Fletcher-Reeves conj. grad. method
    See Jorge Nocedal and Stephen J. Wright 'Numerical
    Optimization' Second Edition, 2006 (p. 121)
    """

    def __init__(self):
        super(FRcg, self).__init__()

    def __str__(self):
        return 'FRcg'

    def update_data(self, x_k1, g_k1):

        if self.iters == 0:
            self.p_k = -g_k1
            #save the step
            self.g_k = g_k1.copy()
            self.iters += 1

            return self.p_k
        else:

            dot_g_k1_g_k1 = np.dot(g_k1, g_k1)
            dot_g_g = np.dot(self.g_k, self.g_k)
            beta_k = dot_g_k1_g_k1 / dot_g_g
            self.p_k = beta_k * self.p_k - g_k1
            # save this step
            self.g_k = g_k1.copy()
            self.iters += 1

            if self.iters > 10:
                self.iters = 0

            return self.p_k


class PRPcg(SteepestDescent):
    """
    The Polak-Reeves+ conj. grad. method
    See Jorge Nocedal and Stephen J. Wright 'Numerical
    Optimization' Second Edition, 2006
    """

    def __init__(self):
        super(PRPcg, self).__init__()

    def __str__(self):
        return 'Polak-Reeves+ conjugate gradient method'

    def update_data(self, x_k1, g_k1):

        if self.iters == 0:
            self.p_k = -g_k1
            #save the step
            self.g_k = g_k1.copy()
            self.iters += 1

            return self.p_k
        else:
            dg = np.dot(g_k1, g_k1 - self.g_k)
            dot_g_g = np.dot(self.g_k, self.g_k)

            beta_k = dg / dot_g_g
            beta_k = np.maximum(0.0, beta_k)

            self.p_k = beta_k * self.p_k - g_k1
            # save this step
            self.g_k = g_k1.copy()
            self.iters += 1

            return self.p_k


class PRcg(SteepestDescent):
    """
    The Polak-Reeves conj. grad. method
    See Jorge Nocedal and Stephen J. Wright 'Numerical
    Optimization' Second Edition, 2006
    """

    def __init__(self):
        super(PRcg, self).__init__()

    def __str__(self):
        return 'Polak-Reeves conjugate gradient method'

    def update_data(self, x_k1, g_k1):

        if self.iters == 0:
            self.p_k = -g_k1
            #save the step
            self.g_k = g_k1.copy()
            self.iters += 1

            return self.p_k
        else:

            dg = np.dot(g_k1, g_k1 - self.g_k)
            dot_g_g = np.dot(self.g_k, self.g_k)
            beta_k = dg / dot_g_g

            self.p_k = beta_k * self.p_k - g_k1
            # save this step
            self.g_k = g_k1.copy()
            self.iters += 1

            if self.iters > 10:
                self.iters = 0

            return self.p_k


class HZcg(SteepestDescent):

    """
    conjugate gradient method from paper of
    William W. Hager and Hongchao Zhang
    SIAM J. optim., 16(1), 170-192. (23 pages)
    """

    def __init__(self):

        super(HZcg, self).__init__()
        self.eta = 0.01

    def __str__(self):

        return 'Hager-Zhang conjugate gradient method'

    def update_data(self, x_k1, g_k1):

        if self.iters == 0:
            self.p_k = -g_k1
            # save the step
            self.g_k = g_k1.copy()
            self.iters += 1

            return self.p_k
        else:
            y_k = g_k1 - self.g_k
            try:
                dot_yp = np.dot(y_k, self.p_k)
                rho = 1.0 / dot_yp
            except ZeroDivisionError:
                rho = 1.0e10

            norm2 = np.dot(y_k, y_k)
            y1 = y_k - 2.0 * rho * norm2 * self.p_k

            beta_k = rho * np.dot(y1, g_k1)

            try:
                norm_p = np.sqrt(np.dot(self.p_k, self.p_k))

                norm_g = np.sqrt(np.dot(self.g_k, self.g_k))
                eta_k = - 1.0 / (norm_p * min(self.eta, norm_g))
            except ZeroDivisionError:
                eta_k = 1.0e10
            beta_k = max(beta_k, eta_k)
            self.p_k = self.p_k * beta_k - g_k1
            # save this step
            self.g_k = g_k1.copy()
            self.iters += 1

            if self.iters > 10:
                self.iters = 0

            return self.p_k


class QuickMin(SteepestDescent):

    """
    H. J\'onsson, G. Mills, and K. Jacobsen.
    B.J. Berne, G. Ciccotti, D.F. Coker (Eds.).
    Classical and Quantum Dynamics in
    Condensed Phase Simulations, World Scientific (1998), 385 (1998)
    """

    def __init__(self, dt=0.005, m=0.01):
        super(QuickMin, self).__init__()
        self.dt = dt
        self.m = m

    def __str__(self):

        return 'QuickMin'

    def update_data(self, x_k1, g_k1):
        dt = self.dt
        m = self.m
        if self.iters == 0:
            self.v = 0.0
            p = -0.5 * g_k1 * dt**2.0/m
            self.g = g_k1.copy()
            self.iters += 1
            return p
        else:
            self.v = self.v - 0.5 * (self.g + g_k1) * dt / m
            dot_gv = np.dot(g_k1, self.v)
            dot_gg = np.dot(g_k1, g_k1)
            if dot_gv > 0.0:
                dot_gv = 0.0
            self.v = g_k1 * dot_gv / dot_gg
            p = dt * self.v - 0.5 * g_k1 * dt**2.0 / m
            self.g = g_k1.copy()
            self.iters += 1
            return p


class FlexVPO(SteepestDescent):

    """
    H. J\'onsson, G. Mills, and K. Jacobsen.
    B.J. Berne, G. Ciccotti, D.F. Coker (Eds.).
    Classical and Quantum Dynamics in
    Condensed Phase Simulations, World Scientific (1998), 385 (1998)
    """

    def __init__(self, dt=0.01, m=0.01):
        super(FlexVPO, self).__init__()
        self.dt = dt
        self.m = m

    def __str__(self):

        return 'FlexVPO'

    def update_data(self, x_k1, g_k1):
        dt = self.dt
        m = self.m
        if self.iters == 0:
            self.v = 0.0
            p = -0.5 * g_k1 * dt**2.0/m
            self.g = g_k1.copy()
            self.iters += 1
            return p
        else:
            self.v = self.v - 0.5 * (self.g + g_k1) * dt / m
            dot_gv = np.dot(g_k1, self.v)
            dot_gg = np.dot(g_k1, g_k1)
            if self.iters % 5 == 0:
                self.v = g_k1 * dot_gv / dot_gg
            if dot_gv / dot_gg > 0.0:
                self.v = 0.0
            p = dt * self.v - 0.5 * g_k1 * dt**2.0 / m
            self.g = g_k1.copy()
            self.iters += 1
            return p


class LBFGS2(SteepestDescent):

    """
    The limited-memory BFGS.
    See Jorge Nocedal and Stephen J. Wright 'Numerical
    Optimization' Second Edition, 2006 (p. 177)
    """

    def __init__(self, memory=3):
        """
        :param m: memory (amount of previous steps to use)
        """
        super(LBFGS2, self).__init__()

        self.s_k = {i: None for i in range(memory)}
        self.y_k = {i: None for i in range(memory)}

        self.rho_k = np.zeros(shape=memory)

        self.kp = {}
        self.p = 0
        self.k = 0

        self.m = memory

        self.stable = True

    def __str__(self):

        return 'LBFGS2'

    def update_data(self, x_k1, g_k1):

        if self.k == 0:

            self.kp[self.k] = self.p
            self.x_k = x_k1.copy()
            self.g_k = g_k1.copy()

            self.s_k[self.kp[self.k]] = np.zeros_like(g_k1)
            self.y_k[self.kp[self.k]] = np.zeros_like(g_k1)

            self.k += 1
            self.p += 1

            self.kp[self.k] = self.p

            p = -g_k1
            self.iters += 1

            return p

        else:

            if self.p == self.m:
                self.p = 0
                self.kp[self.k] = self.p

            s_k = self.s_k
            x_k = self.x_k
            y_k = self.y_k
            g_k = self.g_k

            x_k1 = x_k1.copy()

            rho_k = self.rho_k

            kp = self.kp
            k = self.k
            m = self.m

            s_k[kp[k]] = x_k1 - x_k
            y_k[kp[k]] = g_k1 - g_k

            dot_ys = np.dot(y_k[kp[k]], s_k[kp[k]])

            if abs(dot_ys) > 0.0:
                rho_k[kp[k]] = 1.0 / dot_ys
            else:
                rho_k[kp[k]] = 1.0e16

            if rho_k[kp[k]] < 0.0:
                self.stable = False
                self.__init__(memory=self.m)
                return self.update_data(x_k1, g_k1)

            q = g_k1.copy()

            alpha = np.zeros(np.minimum(k + 1, m))
            j = np.maximum(-1, k - m)

            for i in range(k, j, -1):
                dot_sq = np.dot(s_k[kp[i]], q)

                alpha[kp[i]] = rho_k[kp[i]] * dot_sq

                q -= alpha[kp[i]] * y_k[kp[i]]

            t = k
            dot_yy = np.dot(y_k[kp[t]], y_k[kp[t]])

            if abs(dot_yy) > 1.0e-10:
                r = q / (rho_k[kp[t]] * dot_yy)
            else:
                r = q

            for i in range(np.maximum(0, k - m + 1), k + 1):
                dot_yr = np.dot(y_k[kp[i]], r)

                beta = rho_k[kp[i]] * dot_yr
                r += s_k[kp[i]] * (alpha[kp[i]] - beta)

            # save this step:
            self.x_k = x_k1.copy()
            self.g_k = g_k1.copy()
            self.k += 1
            self.p += 1
            self.kp[self.k] = self.p
            self.iters += 1

            return -1.0 * r


class LBFGS(SteepestDescent):

    """
    The limited-memory BFGS.
    See Jorge Nocedal and Stephen J. Wright 'Numerical
    Optimization' Second Edition, 2006 (p. 177)
    """

    def __init__(self, memory=3):
        """
        :param m: memory (amount of previous steps to use)
        """
        super(LBFGS, self).__init__()

        self.s_k = {i: None for i in range(memory)}
        self.y_k = {i: None for i in range(memory)}

        self.rho_k = np.zeros(shape=memory)

        self.kp = {}
        self.p = 0
        self.k = 0

        self.m = memory

        self.stable = True

    def __str__(self):

        return 'LBFGS'

    def update_data(self, x_k1, g_k1):

        """

        :param x_k1: is a search direction
        :param g_k1: is gradients
        :return:
        """

        if self.k == 0:

            self.kp[self.k] = self.p
            self.g_k = g_k1.copy()

            self.s_k[self.kp[self.k]] = np.zeros_like(g_k1)
            self.y_k[self.kp[self.k]] = np.zeros_like(g_k1)

            self.k += 1
            self.p += 1
            self.kp[self.k] = self.p

            self.iters += 1

            return - g_k1

        else:

            if self.p == self.m:
                self.p = 0
                self.kp[self.k] = self.p

            s_k = self.s_k
            y_k = self.y_k
            g_k = self.g_k
            rho_k = self.rho_k

            kp = self.kp
            k = self.k
            m = self.m

            s_k[kp[k]] = x_k1
            y_k[kp[k]] = g_k1 - g_k

            dot_ys = np.dot(y_k[kp[k]], s_k[kp[k]])

            if abs(dot_ys) > 0.0:
                rho_k[kp[k]] = 1.0 / dot_ys
            else:
                rho_k[kp[k]] = 1.0e40

            if rho_k[kp[k]] < 0.0:
                self.stable = False
                self.__init__(memory=self.m)
                return self.update_data(x_k1, g_k1)

            q = g_k1.copy()

            alpha = np.zeros(np.minimum(k + 1, m))
            j = np.maximum(-1, k - m)

            for i in range(k, j, -1):
                dot_sq = np.dot(s_k[kp[i]], q)
                alpha[kp[i]] = rho_k[kp[i]] * dot_sq
                q -= alpha[kp[i]] * y_k[kp[i]]

            t = k
            dot_yy = np.dot(y_k[kp[t]], y_k[kp[t]])

            ryy = rho_k[kp[t]] * dot_yy
            if abs(ryy) > 0.0:
                r = q / (ryy)
            else:
                r = q * 1.0e40

            for i in range(np.maximum(0, k - m + 1), k + 1):
                dot_yr = np.dot(y_k[kp[i]], r)
                beta = rho_k[kp[i]] * dot_yr
                r += s_k[kp[i]] * (alpha[kp[i]] - beta)

            # save this step:
            self.g_k = g_k1.copy()
            self.k += 1
            self.p += 1
            self.kp[self.k] = self.p
            self.iters += 1

            return -1.0 * r


class LSR1P(SteepestDescent):

    def __init__(self, memory=10, method='LSR1', phi=None):
        """
        :param m: memory (amount of previous steps to use)
        """
        super(LSR1P, self).__init__()

        self.u_k = {i: None for i in range(memory)}
        self.j_k = {i: None for i in range(memory)}
        self.yj_k = np.zeros(shape=memory)
        self.method = method
        self.phi = phi

        self.phi_k = np.zeros(shape=memory)
        if self.phi is None:
            assert self.method in ['LSR1', 'LP',
                                   'LBofill', 'Linverse_Bofill'], \
                'Value Error'
            if self.method == 'LP':
                self.phi_k.fill(1.0)
        else:
            self.phi_k.fill(self.phi)

        self.kp = {}
        self.p = 0
        self.k = 0

        self.m = memory

    def __str__(self):

        return 'LSR1P'

    def update_data(self, x_k1, g_k1):

        bg_k1 = g_k1.copy()

        if self.k == 0:
            self.kp[self.k] = self.p
            self.x_k = x_k1.copy()
            self.g_k = g_k1.copy()
            self.u_k[self.kp[self.k]] = np.zeros_like(g_k1)
            self.j_k[self.kp[self.k]] = np.zeros_like(g_k1)
            self.k += 1
            self.p += 1
            self.kp[self.k] = self.p
            p = -g_k1
            self.iters += 1

            return p

        else:
            if self.p == self.m:
                self.p = 0
                self.kp[self.k] = self.p

            x_k = self.x_k
            g_k = self.g_k
            u_k = self.u_k
            j_k = self.j_k
            yj_k = self.yj_k
            phi_k = self.phi_k

            x_k1 = x_k1.copy()

            kp = self.kp
            k = self.k
            m = self.m

            s_k = x_k1 - x_k
            y_k = g_k1 - g_k
            by_k = y_k.copy()

            by_k = self.update_bv(by_k, y_k, u_k, j_k, yj_k, phi_k,
                                  np.maximum(1, k - m), k)

            j_k[kp[k]] = s_k - by_k
            yj_k[kp[k]] = np.dot(y_k, j_k[kp[k]])

            dot_yy = np.dot(y_k, y_k)
            if abs(dot_yy) > 1.0e-15:
                u_k[kp[k]] = y_k / dot_yy
            else:
                u_k[kp[k]] = y_k * 1.0e15

            if self.method == 'LBofill' and self.phi is None:
                jj_k = np.dot(j_k[kp[k]], j_k[kp[k]])
                phi_k[kp[k]] = 1 - yj_k[kp[k]]**2 / (dot_yy * jj_k)
            elif self.method == 'Linverse_Bofill' and self.phi is None:
                jj_k =np.dot(j_k[kp[k]], j_k[kp[k]])
                phi_k[kp[k]] = yj_k[kp[k]] ** 2 / (dot_yy * jj_k)

            bg_k1 = self.update_bv(bg_k1, g_k1, u_k, j_k, yj_k, phi_k,
                                   np.maximum(1, k - m + 1), k + 1)

            # save this step:
            self.x_k = x_k1.copy()
            self.g_k = g_k1.copy()
            self.k += 1
            self.p += 1
            self.kp[self.k] = self.p
            self.iters += 1

        return -bg_k1

    def update_bv(self, bv, v, u_k, j_k, yj_k, phi_k, i_0, i_m):
        kp = self.kp

        for i in range(i_0, i_m):
            dot_uv = np.dot(u_k[kp[i]], v)
            dot_jv = np.dot(j_k[kp[i]], v)

            alpha = dot_jv - yj_k[kp[i]] * dot_uv
            beta_p = dot_uv*j_k[kp[i]] + alpha*u_k[kp[i]]

            beta_ms = j_k[kp[i]] * (dot_jv / yj_k[kp[i]])

            beta = (1-phi_k[kp[i]])*beta_ms + phi_k[kp[i]] * beta_p

            bv = bv + beta

        return bv
