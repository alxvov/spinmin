import numpy as np


def calc_heis_energy(J, spins, nbl_one_way):

    n_atoms = spins.shape[0]
    energy = 0.0

    for i in range(n_atoms):
        nl_indices = nbl_one_way.get_neighbors(i)[0]
        for j in nl_indices:
            energy += np.dot(spins[i], spins[j])

    return -1.0 * J * energy


def calc_dm_energy(D, atoms, spins, nbl_one_way):

    n_atoms = spins.shape[0]
    energy = 0.0

    for i in range(n_atoms):
        nl_indices, offsets = nbl_one_way.get_neighbors(i)
        for j, offset in zip(nl_indices, offsets):
            cross_ij = np.cross(spins[i], spins[j])
            r_ij = atoms[i].position - \
                   (atoms[j].position +
                    np.dot(offset, atoms.get_cell()))
            r_ij /= np.sqrt(np.dot(r_ij, r_ij))
            energy += np.dot(r_ij, cross_ij)

    return -1.0 * D * energy


def calc_anisotropy_energy(K_ad, spins):

    K, e_an = K_ad['ampl'], K_ad['dir']
    energy = 0.0
    n_atoms = spins.shape[0]

    for i in range(n_atoms):
        energy += np.dot(e_an, spins[i])**2

    return -K * energy


def calc_zeeman_energy(Z_ad, spins):

    Z, b_vec = Z_ad['ampl'], np.asarray(Z_ad['dir'])
    energy = 0.0
    n_atoms = spins.shape[0]

    for i in range(n_atoms):
        energy += np.dot(b_vec, spins[i])

    return - Z * energy


def calc_heis_grad(J, spins, nbl_both_way):

    n_atoms = spins.shape[0]
    grad = np.zeros(shape=(n_atoms, 3))

    for i in range(n_atoms):
        nl_indices = nbl_both_way.get_neighbors(i)[0]
        for j in nl_indices:
            grad[i] += spins[j]

    return -1.0 * J * grad


def calc_dm_grad(D, atoms, spins, nbl_both_way):

    n_atoms = spins.shape[0]
    grad = np.zeros(shape=(n_atoms, 3))

    for i in range(n_atoms):
        nl_indices, offsets = nbl_both_way.get_neighbors(i)
        for j, offset in zip(nl_indices, offsets):
            r_ij = atoms[i].position - \
                   (atoms[j].position +
                    np.dot(offset, atoms.get_cell()))
            r_ij /= np.sqrt(np.dot(r_ij, r_ij))
            grad[i] += np.cross(spins[j], r_ij)

    return -1.0 * D * grad


def calc_anisotropy_grad(K_ad, spins):

    K, e_an = K_ad['ampl'], np.asarray(K_ad['dir'])
    n_atoms = spins.shape[0]
    grad = np.zeros(shape=(n_atoms, 3))

    for i in range(n_atoms):
        grad[i] += 2.0 * np.dot(e_an, spins[i]) * e_an

    return -K * grad


def calc_zeeman_grad(Z_ad, n_atoms):

    Z, b_vec = Z_ad['ampl'], Z_ad['dir']
    grad = np.zeros(shape=(n_atoms, 3))

    for i in range(n_atoms):
        grad[i] += b_vec

    return -Z * grad

def calc_i44_energy_and_grad(I44, spins, n_atoms):
    m1 = 0
    m2 = 0
    m3 = 0
    m4 = 0
    energy = 0.0
    grad = np.zeros(shape=(n_atoms, 3))
    N = int(np.sqrt(n_atoms))

    for i in range(n_atoms):
        #  _
        # /_/
        if (i+1) % N == 0 and  N - 1 <= i <= (N-1)*N - 1:
            m1 = i
            m2 = i - (N-1)
            m3 = i + N - (N-1)
            m4 = i + N
        elif N*(N - 1) <= i < N*(N - 1) + N -1:
            m1 = i
            m2 = i + 1
            m3 = i - (N**2 - N) + 1
            m4 = i - (N**2 - N)
        elif i == N*(N-1) + (N-1):
            m1 = i
            m2 = i - (N-1)
            m3 = i - (N-1) - (N*N-N)
            m4 = i - (N*N - N)
        else:
            m1 = i
            m2 = i + 1
            m3 = i + 1 + N
            m4 = i + N

        energy += \
                (spins[m1] @ spins[m2]) * (spins[m3] @ spins[m4]) + \
                (spins[m1] @ spins[m4]) * (spins[m2] @ spins[m3]) - \
                (spins[m1] @ spins[m3]) * (spins[m2] @ spins[m4])

        grad[m1] += spins[m2] * (spins[m3] @ spins[m4]) + \
                   spins[m4] * (spins[m2] @ spins[m3]) - \
                   spins[m3] * (spins[m2] @ spins[m4])

        grad[m2] += spins[m1] * (spins[m3] @ spins[m4]) + \
                   (spins[m1] @ spins[m4]) *  spins[m3] - \
                   (spins[m1] @ spins[m3]) * spins[m4]

        grad[m3] += (spins[m1] @ spins[m2]) * spins[m4] + \
                   (spins[m1] @ spins[m4]) * spins[m2] - \
                   spins[m1] * (spins[m2] @ spins[m4])

        grad[m4] += (spins[m1] @ spins[m2]) * spins[m3] + \
                   spins[m1] * (spins[m2] @ spins[m3]) - \
                   (spins[m1] @ spins[m3]) * spins[m2]


        #  _
        # \_\

        if i % N == 0 and 0 <= i // N <= N - 2:
            m1 = i
            m2 = i + 1
            m3 = i + N
            m4 = i + N + (N - 1)
        elif N * (N - 1) + 1 <= i < N * (N - 1) + N - 1:
            m1 = i
            m2 = i + 1
            m3 = i - (N**2 - N)
            m4 = i - (N**2 - N) - 1
        elif (i+1) % N == 0 and  N - 1 <= i <= (N-1)*N - 1:
            m1 = i
            m2 = i - (N - 1)
            m3 = i + N
            m4 = i + N - 1
        elif i == (N + 1)* (N - 1):
            m1 = i
            m2 = i - (N - 1)
            m3 = i - (N + 1)* (N - 1) + N - 1
            m4 = i - (N + 1)* (N - 1) + N - 1 - 1
        elif i == N * (N - 1):
            m1 = i
            m2 = i + 1
            m3 = i - (N - 1) * N
            m4 = i - (N - 1) * N + (N - 1)
        else:
            m1 = i
            m2 = i + 1
            m3 = i + N
            m4 = i + N - 1

        energy += \
                (spins[m1] @ spins[m2]) * (spins[m3] @ spins[m4]) + \
                (spins[m1] @ spins[m4]) * (spins[m2] @ spins[m3]) - \
                (spins[m1] @ spins[m3]) * (spins[m2] @ spins[m4])

        grad[m1] += spins[m2] * (spins[m3] @ spins[m4]) + \
                   spins[m4] * (spins[m2] @ spins[m3]) - \
                   spins[m3] * (spins[m2] @ spins[m4])

        grad[m2] += spins[m1] * (spins[m3] @ spins[m4]) + \
                   (spins[m1] @ spins[m4]) *  spins[m3] - \
                   (spins[m1] @ spins[m3]) * spins[m4]

        grad[m3] += (spins[m1] @ spins[m2]) * spins[m4] + \
                   (spins[m1] @ spins[m4]) * spins[m2] - \
                   spins[m1] * (spins[m2] @ spins[m4])

        grad[m4] += (spins[m1] @ spins[m2]) * spins[m3] + \
                   spins[m1] * (spins[m2] @ spins[m3]) - \
                   (spins[m1] @ spins[m3]) * spins[m2]


        # /\
        # |/
        if i % N == 0 and 0 <= i // N <= N - 3:
            m1 = i
            m2 = i + N
            m3 = i + 2 * N - 1 + N
            m4 = i + 2 * N - 1

        elif i % N == 0 and i // N == N - 2:
            m1 = i
            m2 = i + N
            m3 = i - N * (N - 2) + N - 1
            m4 = i + N + N - 1
        elif i == N * (N - 1):
            m1 = i
            m2 = i - (N - 1) * N
            m3 = i - (N - 1) * N + (N - 1) + N
            m4 = i - (N - 1) * N + (N - 1)
        elif N * (N - 1) + 1 <= i <= N * (N - 1) + N - 1:
            m1 = i
            m2 = i - (N ** 2 - N)
            m3 = i - (N - 1) * N + N - 1
            m4 = i - (N - 1) * N - 1
        elif N * (N - 2) + 1 <= i < N * (N - 1) + N - 1:
            m1 = i
            m2 = i + N
            m3 = i + N - 1 - (N - 1) * N
            m4 = i + N - 1
        else:
            m1 = i
            m2 = i + N
            m3 = i + 2 * N - 1
            m4 = i + N - 1

        energy += \
                (spins[m1] @ spins[m2]) * (spins[m3] @ spins[m4]) + \
                (spins[m1] @ spins[m4]) * (spins[m2] @ spins[m3]) - \
                (spins[m1] @ spins[m3]) * (spins[m2] @ spins[m4])

        grad[m1] += spins[m2] * (spins[m3] @ spins[m4]) + \
                   spins[m4] * (spins[m2] @ spins[m3]) - \
                   spins[m3] * (spins[m2] @ spins[m4])

        grad[m2] += spins[m1] * (spins[m3] @ spins[m4]) + \
                   (spins[m1] @ spins[m4]) *  spins[m3] - \
                   (spins[m1] @ spins[m3]) * spins[m4]

        grad[m3] += (spins[m1] @ spins[m2]) * spins[m4] + \
                   (spins[m1] @ spins[m4]) * spins[m2] - \
                   spins[m1] * (spins[m2] @ spins[m4])

        grad[m4] += (spins[m1] @ spins[m2]) * spins[m3] + \
                   spins[m1] * (spins[m2] @ spins[m3]) - \
                   (spins[m1] @ spins[m3]) * spins[m2]

    return -I44 * energy, -I44 * grad


def calc_i34_energy_and_grad(I34, spins, n_atoms):
    m1 = 0
    m2 = 0
    m3 = 0
    energy = 0.0
    grad = np.zeros(shape=(n_atoms, 3))
    N = int(np.sqrt(n_atoms))

    for i in range(n_atoms):
        #
        # /\
        # -
        if (i+1) % N == 0 and  N - 1 <= i <= (N-1)*N - 1:
            m1 = i
            m2 = i - (N-1)
            m3 = i + N

        elif N*(N - 1) <= i <= N*(N - 1) + N - 2:
            m1 = i
            m2 = i + 1
            m3 = i - (N - 1)*N

        elif i == (N + 1)*(N-1):
            m1 = i
            m2 = i - (N-1)
            m3 = i - (N-1) * N

        else:
            m1 = i
            m2 = i + 1
            m3 = i + N


        energy += \
                (spins[m2] @ spins[m1]) * (spins[m1] @ spins[m3]) + \
                (spins[m1] @ spins[m2]) * (spins[m2] @ spins[m3]) + \
                (spins[m1] @ spins[m3]) * (spins[m3] @ spins[m2])

        grad[m1] += \
                    spins[m2] * (spins[m1] @ spins[m3]) + \
                    (spins[m2] @ spins[m1]) * spins[m3] + \
                    spins[m2] * (spins[m2] @ spins[m3]) + \
                    spins[m3] * (spins[m3] @ spins[m2])

        grad[m2] +=\
                (spins[m1]) * (spins[m1] @ spins[m3]) + \
                (spins[m1]) * (spins[m2] @ spins[m3]) + \
                (spins[m1] @ spins[m2]) * (spins[m3]) + \
                (spins[m1] @ spins[m3]) * (spins[m3])

        grad[m3] +=\
                (spins[m2] @ spins[m1]) * (spins[m1]) + \
                (spins[m1] @ spins[m2]) * (spins[m2]) + \
                (spins[m1]) * (spins[m3] @ spins[m2]) + \
                (spins[m1] @ spins[m3]) * (spins[m2])

        #  _
        # \/
        #

        if i % N == 0 and 0 <= i // N <= N - 2:
            m1 = i
            m2 = i + N
            m3 = i + N + (N - 1)

        elif i == N * (N - 1):
            m1 = i
            m2 = i - (N - 1) * N
            m3 = i - (N - 1) * N + (N - 1)

        elif N * (N - 1) + 1 <= i <= N * (N - 1) + N - 1:
            m1 = i
            m2 = i - (N - 1) * N
            m3 = i - (N - 1) * N - 1

        else:
            m1 = i
            m2 = i + N
            m3 = i + N - 1

        energy += \
                (spins[m2] @ spins[m1]) * (spins[m1] @ spins[m3]) + \
                (spins[m1] @ spins[m2]) * (spins[m2] @ spins[m3]) + \
                (spins[m1] @ spins[m3]) * (spins[m3] @ spins[m2])

        grad[m1] += \
                    spins[m2] * (spins[m1] @ spins[m3]) + \
                    (spins[m2] @ spins[m1]) * spins[m3] + \
                    spins[m2] * (spins[m2] @ spins[m3]) + \
                    spins[m3] * (spins[m3] @ spins[m2])

        grad[m2] +=\
                (spins[m1]) * (spins[m1] @ spins[m3]) + \
                (spins[m1]) * (spins[m2] @ spins[m3]) + \
                (spins[m1] @ spins[m2]) * (spins[m3]) + \
                (spins[m1] @ spins[m3]) * (spins[m3])

        grad[m3] +=\
                (spins[m2] @ spins[m1]) * (spins[m1]) + \
                (spins[m1] @ spins[m2]) * (spins[m2]) + \
                (spins[m1]) * (spins[m3] @ spins[m2]) + \
                (spins[m1] @ spins[m3]) * (spins[m2])

    return -I34 * energy, -I34 * grad

def calc_i24_energy_and_grad(I24, spins, n_atoms):
    m1 = 0
    m2 = 0
    energy = 0.0
    grad = np.zeros(shape=(n_atoms, 3))
    N = int(np.sqrt(n_atoms))

    for i in range(n_atoms):
        #
        # -
        if (i+1) % N == 0 and  N - 1 <= i <= (N-1)*N + N - 1:
            m1 = i
            m2 = i - (N-1)
        else:
            m1 = i
            m2 = i + 1

        scal = (spins[m1] @ spins[m2])
        energy += scal**2
        grad[m1] += 2.0 * spins[m2] * scal
        grad[m2] += 2.0 * spins[m1] * scal

        #
        # /
        #

        if N * (N - 1) <= i <= N * (N - 1) + N - 1:
            m1 = i
            m2 = i - (N - 1) * N
        else:
            m1 = i
            m2 = i + N

        scal = (spins[m1] @ spins[m2])
        energy += scal**2
        grad[m1] += 2.0 * spins[m2] * scal
        grad[m2] += 2.0 * spins[m1] * scal

        #
        # |
        #

        if i % N == 0 and 0 <= i // N <= N - 2:
            m1 = i
            m2 = i + N + (N - 1)
        elif i == N * (N - 1):
            m1 = i
            m2 = i - (N - 1) * N + N - 1
        elif N * (N - 1) + 1 <= i <= N * (N - 1) + N - 1:
            m1 = i
            m2 = i - (N - 1) * N - 1
        else:
            m1 = i
            m2 = i + N - 1

        scal = (spins[m1] @ spins[m2])
        energy += scal**2
        grad[m1] += 2.0 * spins[m2] * scal
        grad[m2] += 2.0 * spins[m1] * scal

    return -I24 * energy, -I24 * grad