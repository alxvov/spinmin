from .rk56 import RK56
from .sib import SIB
import time
import numpy as np

class SpinDynamics:

    def __init__(self, hamiltonian, dyn_algo='SIB',
                 dt=200.0, alpha=0.1, eq_type='LLG',
                 temperature=None, n_steps=10000,
                 damping_only=False,
                 convergence=1e-5,
                 logevery=np.inf,
                 txt=None):

        self.ham = hamiltonian
        self.dyn_algo = dyn_algo
        self.dt = dt  # fs
        self.alpha = alpha
        self.damping_only = damping_only
        self.temperature = temperature  # meV
        self.eq_type = eq_type
        self.txt = txt

        if self.dyn_algo == 'RK56':
            self.algo = RK56(eq_type, alpha, dt, temperature, damping_only)
        elif self.dyn_algo == 'SIB':
            self.algo = SIB(eq_type, alpha, dt, temperature, damping_only)

        self.n_steps = n_steps
        self.global_iters = 0
        self.convergence = convergence
        self.einit = None
        self.efinal = None
        self.twall = None
        self.iters = 0
        self.logevery=logevery
    def run(self):

        if self.txt is not None:
            file=open(self.txt, "w")
        else:
            file=None
        self.einit = self.ham.get_energy_and_gradients()[0]
        t1 = time.time()

        while True:
            self.algo.update(self.ham)

            # print every 50th iteration
            if self.global_iters % self.logevery == 0:
                if self.ham.type == 'single_image':
                    expm = self.ham.spins.sum(axis=0)/len(self.ham.atoms)
                    print(self.global_iters, self.ham.e_total,
                          expm[0], expm[1], expm[2],
                          self.algo.error, file=file)
                else:
                    print(self.global_iters, self.ham.e_total,
                          max(self.algo.error))

            # check max iteration, and convergence criterion
            A = self.global_iters >= self.n_steps
            B = False
            if self.ham.type == 'multi_images':
                if self.global_iters % 10 == 0:
                    self.ham.set_climbing_image()

                for v in range(self.ham.noi-2):
                    if v+1 == self.ham.CI:
                        B = self.algo.error[v] < self.convergence and self.temperature is None
                    else:
                        B = self.algo.error[v] < self.convergence*10 and self.temperature is None
                    if not B:
                        break
            else:
                B = self.algo.error < self.convergence and self.temperature is None

            self.global_iters += 1
            self.iters = self.global_iters
            if A or B:
                print(self.global_iters, self.algo.error)
                break

        self.efinal = self.ham.e_total
        if self.dyn_algo == 'SIB':
            self.eg_counter = 2 * self.iters
        elif self.dyn_algo == 'RK56':
            self.eg_counter = 6 * self.iters
        t2 = time.time()
        self.twall = t2 - t1

        if file is not None:
            file.close()