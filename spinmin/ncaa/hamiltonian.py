from ase.neighborlist import NeighborList, NewPrimitiveNeighborList
import numpy as np
import ncaa_module
from spinmin.minimise import search_direction
import energy_module
import gradient_module
from datetime import datetime


class NCAAHamiltonian:

    def __init__(self, atoms, spins, convergence=1.0e-10,
                 mixing=1.0, maxiter=1000, maxmove=0.04,
                 method='LBFGS2', txt='output.txt',
                 use_constr=False, interactions=None, tol2=1.0e-8,
                 init_n=None, init_m=None, tags=None,
                 full_output=False, logappend=True, logevery=np.inf):

        print('Initialization...')

        msg = "Your spins array is not contiguous, " \
              "please converte them to contiguous arrays" \
              "(see numpy docs)"
        assert spins.flags['C_CONTIGUOUS'], msg

        self.spins = spins
        self.atoms = atoms
        self.noa = len(atoms)
        self.type = 'single_image'
        self.tags = tags

        self.interactions = interactions

        # self.r_c = [interactions['r_c']] * len(atoms)
        self.E0 = interactions.get('E0')
        self.V = interactions.get('V')
        self.U = interactions.get('U')
        self.Gamma = interactions.get('Gamma')
        self.Z_ad = interactions.get('Z_ad')
        self.DM = interactions.get('DM')
        self.K_ad = interactions.get('K_ad')
        self.K2_ad = interactions.get('K2_ad')

        assert isinstance(self.V, list) or isinstance(self.V, float)

        if self.Z_ad is None:
            self.Z_ad = {'ampl': 0.0, 'dir': [0.0, 0.0, 0.0]}
        if self.K_ad is None:
            self.K_ad = {'ampl': 0.0, 'dir': [0.0, 0.0, 0.0]}
        if self.K2_ad is None:
            self.K2_ad = {'ampl': 0.0, 'dir': [0.0, 0.0, 0.0]}

        self.tol = convergence
        self.tol2 = tol2
        self.method = method
        self.mixing = mixing
        self.maxiter = maxiter
        self.txt = txt
        self.maxmove = maxmove

        # generate list of neighbour distances
        p0 = atoms.positions[0]
        dist = np.asarray(
            [np.linalg.norm(p0 - p) for p in atoms.positions])
        inds = np.argsort(dist)
        new_dist = [0.0]
        new_inds = [0]
        for j in inds:
            if (dist[j] - new_dist[-1]) < 1.0e-2:
                continue
            else:
                new_dist.append(dist[j])
                new_inds.append(j)
        if isinstance(self.V, float):
            self.V = np.asarray([self.V])
        self.V = np.asarray(self.V)
        my_ind = new_inds[1:len(self.V) + 1]
        self.nndist = dist[my_ind]
        self.r_c = self.nndist[-1]

        # neighbours
        print('Building neighbour list...')

        self.nbl_one_way = NeighborList(self.r_c, skin=0.01,
                                        self_interaction=False,
                                        bothways=False,
                                        primitive=NewPrimitiveNeighborList)

        self.nbl_both_way = NeighborList(self.r_c, skin=0.01,
                                         self_interaction=False,
                                         bothways=True,
                                         primitive=NewPrimitiveNeighborList)

        self.nbl_one_way.update(atoms)
        self.nbl_both_way.update(atoms)

        self.nbl1_ow = []
        self.nbl2_ow = []
        self.offsets_ow = []
        for i in range(len(atoms)):
            nl_indices, offsets = self.nbl_one_way.get_neighbors(i)
            for j, z in zip(nl_indices, offsets):
                self.nbl1_ow.append(i)
                self.nbl2_ow.append(j)
                self.offsets_ow.append(z.tolist())
        self.nbl1_ow = np.array(self.nbl1_ow)
        self.nbl2_ow = np.array(self.nbl2_ow)
        self.offsets_ow = np.asarray(self.offsets_ow, dtype=float)

        self.nbl1_bw = []
        self.nbl2_bw = []
        self.offsets_bw = []
        for i in range(len(atoms)):
            nl_indices, offsets = self.nbl_both_way.get_neighbors(i)
            for j, z in zip(nl_indices, offsets):
                self.nbl1_bw.append(i)
                self.nbl2_bw.append(j)
                self.offsets_bw.append(z.tolist())
        self.nbl1_bw = np.array(self.nbl1_bw)
        self.nbl2_bw = np.array(self.nbl2_bw)
        self.offsets_bw = np.asarray(self.offsets_bw, dtype=float)

        self.nmthphi = np.ones(shape=(4, len(atoms)))
        # self.nmthphi[0,:] *= 1.4
        # self.nmthphi[1,:] *= 0.44
        if init_n is None:
            self.nmthphi[0,:] *= 1.4270320892200323
        else:
            self.nmthphi[0, :] = init_n.copy()
        if init_m is None:
            self.nmthphi[1,:] *=  0.4555758399097601
        else:
            self.nmthphi[1, :] = init_m.copy()

        print('Finished building neighbour list...')

        self.constr = np.zeros(shape=(2 * len(atoms)))
        self.use_constr = use_constr

        # self.energy_heiss = 0.0
        # self.energy_dm = 0.0
        # self.energy_anis = 0.0
        # self.energy_anis2 = 0.0
        # self.energy_zee = 0.0
        # calculate length of spins and normalize it
        self.mm = []
        for i, s in enumerate(self.spins):
            x = np.sqrt(np.dot(s, s))
            s /= x
            if i > 0:
                if abs(x - self.mm[-1]) > 1.0e-6:
                    raise ValueError('Only spins with equal'
                                     ' length are accepted')
            self.mm.append(x)
        self.eig = None
        self.eigv = None

        self.total_iters = 0

        self.appendlogfile = logappend
        self.txt = txt
        if self.txt is not None:
            if self.appendlogfile:
                dotofile = "a"
            else:
                dotofile = "w"

            self.file = open(self.txt, dotofile)
        else:
            self.file = None
        self.logevery = logevery

        self.initial_output()
        self.full_output=full_output
        print('Initialization done.\n', file=self.file)

    def get_energy_and_gradients(self):
        self.directmin_mz_only()
        self.e_total = self.energy
        if self.tags is not None:
            self.forces[self.tags] = np.zeros_like(self.forces[self.tags])
        return self.e_total, -self.forces

    # def get_energy(self):
    #     # self.scf()
    #     self.directmin_mz_only()
    #     # self.directmin_mz_only()
    #     self.e_total = self.energy
    #     return self.energy
    # def get_gradients(self):
    #
    #     if self.tags is not None:
    #         self.forces[self.tags] = np.zeros_like(self.forces[self.tags])
    #     return -self.forces

    def scf(self):
        alpha = self.mixing
        noa = self.noa
        i = 0
        self.forces = np.zeros_like(self.spins)

        while True:

            n, m_x, m_y, m_z, eig, eigv = self.get_n_and_moms()
            m = m_x * self.spins[:,0] + m_y * self.spins[:,1] + m_z * self.spins[:,2]
            dn = np.max(np.absolute(n - self.nmthphi[0,:]))
            dm = np.max(np.absolute(m - self.nmthphi[1,:]))
            self.nmthphi[0,:] = (1.0 - alpha) * self.nmthphi[0,:]  + alpha * n
            self.nmthphi[1,:] = (1.0 - alpha) *  self.nmthphi[1,:] + alpha * m

            if self.use_constr:
                eth, ephi = tangent_vector_from_crts(self.spins)
                g1 = -(m_x * eth[0] + m_y * eth[1] + m_z * eth[2])
                g2 = -(m_x * ephi[0] + m_y * ephi[1] + m_z * ephi[2])
                self.constr[:noa] = self.constr[:noa] + g1 * alpha
                self.constr[noa:] = self.constr[noa:] + g2 * alpha

                self.forces[:, 0] = m_x * 0.5 * self.U * m
                self.forces[:, 1] = m_y * 0.5 * self.U * m
                self.forces[:, 2] = m_z * 0.5 * self.U * m
                dg = np.max(
                    np.linalg.norm(np.cross(self.spins, self.forces), axis=1))

            dx = np.maximum(dn, dm)
            energy = calc_energy(eig, n, m, self.U)
            i += 1
            # if i % 20 == 0:
            #     if self.use_constr:
            #         print(i, dx, dg, energy)
            #     else:
            #         print(i, dx, energy)

            if dx < self.tol or i == self.maxiter:
                if self.use_constr and dg < self.tol2:
                    break
                else:
                    break
            self.total_iters += 1
        # TODO: rewrite it in a more effecient way
        self.spins *= np.sign(m[:,np.newaxis])
        m *= np.sign(m)
        if self.use_constr:
            eth, ephi = tangent_vector_from_crts(self.spins)
            self.forces[:, 0] = -(self.constr[:noa] * eth[0] + self.constr[noa:] * ephi[0]) * m
            self.forces[:, 1] = -(self.constr[:noa] * eth[1] + self.constr[noa:] * ephi[1]) * m
            self.forces[:, 2] = -(self.constr[:noa] * eth[2] + self.constr[noa:] * ephi[2]) * m
        else:
            self.forces[:,0] = m_x * 0.5 * self.U * m
            self.forces[:,1] = m_y * 0.5 * self.U * m
            self.forces[:,2] = m_z * 0.5 * self.U * m
        self.energy = energy  # + self.add_classical_corrections_energy()
        # self.forces += self.add_classical_corrections_forces()
        # self.nmthphi[:2,:] *= np.sign(self.nmthphi[:2,:])
        self.final_output(i, self.maxiter, n, m, energy,
                     np.cross(self.spins, self.forces))
        # print(n, m)

    def directmin(self, mz_only=True):
        # k=0
        scaling_factor = 1.0
        i = 0
        noa = self.noa
        sd_algo = search_direction(self.method)
        if mz_only:
            n_and_m = self.nmthphi[:2,:].reshape(noa*2)
        else:
            n_and_m = self.nmthphi.reshape(noa*4)

        grad = np.zeros_like(n_and_m)
        self.forces = np.zeros_like(self.spins)
        while True:
            n, m_x, m_y, m_z, eig, eigv = self.get_n_and_moms()
            m = m_x * self.spins[:,0] + m_y * self.spins[:,1] + m_z * self.spins[:,2]

            grad[:noa] = (n_and_m[:noa] - n)* (scaling_factor)
            grad[noa:2*noa] = (n_and_m[noa:2*noa] - m) * (scaling_factor)
            if not mz_only:
                th = n_and_m[2*noa:3*noa]
                phi = n_and_m[3*noa:4*noa]

                eth, ephi = tangent_vector(th, phi)
                # grad[2*noa:3*noa] = -m * (m_x * eth[0] + m_y * eth[1] + m_z * eth[2]) * (0.5 * self.U)
                # grad[3*noa:4*noa] = -m * (m_x * ephi[0] + m_y * ephi[1] + m_z * ephi[2]) * (0.5 * self.U)
                grad[2*noa:3*noa] = -(m_x * eth[0] + m_y * eth[1] + m_z * eth[2])
                grad[3*noa:4*noa] = -(m_x * ephi[0] + m_y * ephi[1] + m_z * ephi[2])


            n_and_m_tmp = n_and_m.copy()
            n_and_m_tmp[: 2 * noa] *= scaling_factor

            p_vec = sd_algo.update_data(n_and_m_tmp, grad) * self.mixing
            maxmove = np.max(np.absolute(p_vec))
            if maxmove > self.maxmove * scaling_factor:
                p_vec *= self.maxmove * scaling_factor/maxmove
            n_and_m[: 2 * noa] += p_vec[: 2 * noa] / scaling_factor
            n_and_m[2 * noa:] += p_vec[2 * noa:]

            if not mz_only:
                th = n_and_m[2*noa:3*noa]
                phi = n_and_m[3*noa:4*noa]
                self.spins[:] = sph2cart(th, phi)

            energy = calc_energy(eig, n, m, self.U)
            i += 1
            dx = np.max(np.absolute(grad[:2*noa]))
            dy = 0.0
            if not mz_only:
                self.forces[:, 0] = m * m_x * (0.5 * self.U)
                self.forces[:, 1] = m * m_y * (0.5 * self.U)
                self.forces[:, 2] = m * m_z * (0.5 * self.U)
                dy = np.max(np.absolute(np.cross(self.spins, self.forces)))
            if i % 50 == 0 or i == 1:
                print(i, dx/scaling_factor, dy, energy)
            # if dy < 1.0e-5 and not mz_only and k == 0:
            #     # self.directmin(mz_only=True)
            #     print('DID IT Aaaaaaa')
            #     self.scf()
            #     k+=1
            self.total_iters += 1
            if (dx/scaling_factor < self.tol and dy < self.tol2) or i == self.maxiter:
                break

        # TODO: rewrite it in a more effecient way
        m = n_and_m[noa:noa*2]
        self.spins *= np.sign(m[:,np.newaxis])
        m *= np.sign(m)

        self.forces[:,0] = m * m_x * 0.5 * self.U
        self.forces[:,1] = m * m_y * 0.5 * self.U
        self.forces[:,2] = m * m_z * 0.5 * self.U
        self.energy = energy
        n_and_m[:2*noa] *= np.sign(n_and_m[:2*noa])
        if mz_only:
            self.nmthphi[:2,] = n_and_m.reshape(2, noa)
        else:
            self.nmthphi[:] = n_and_m.reshape(4, noa)

        # print(n, m)
        self.final_output(i, self.maxiter, n, m, energy,
                     np.cross(self.spins, self.forces))

        print(i, dx / scaling_factor, dy, energy)

    def get_n_and_moms(self):
        noa = self.noa
        ham = np.zeros(shape=(2 * noa, 2 * noa), dtype=complex)
        Z, e_zdir = self.Z_ad['ampl'], np.asarray(self.Z_ad['dir'])
        K, e_kdir = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
        K2, e_k2dir = self.K2_ad['ampl'], np.asarray(self.K2_ad['dir'])

        cell = self.atoms.get_cell()
        if not isinstance(cell, np.ndarray):
            cell = cell.array
        nndist=self.nndist

        ncaa_module.fillham(self.E0, self.U, self.V,
                            Z, e_zdir, K, e_kdir, K2, e_k2dir,
                            ham,
                            self.constr,
                            self.spins,
                            self.nmthphi,
                            self.nbl1_ow,
                            self.nbl2_ow,
                            nndist,
                            self.atoms.positions,
                            self.offsets_ow, cell)

        eig, eigv = np.linalg.eigh(ham)

        green_functions = 0.5 - np.arctan(eig) / np.pi

        # number of d electrons
        temp = np.abs(eigv[:noa, :]) ** 2.0 + \
               np.abs(eigv[noa:, :]) ** 2.0
        n = temp @ green_functions

        # m_x
        temp = 2.0 * np.real(eigv[noa:, :] * eigv[:noa, :].conj())
        m_x = temp @ green_functions
        # m_y
        temp = -2.0 * np.imag(eigv[noa:, :] * eigv[:noa, :].conj())
        m_y = temp @ green_functions
        # m_z
        temp = -np.abs(eigv[:noa, :]) ** 2.0 + \
               np.abs(eigv[noa:, :]) ** 2.0
        m_z = temp @ green_functions

        return n, m_x, m_y, m_z, eig, eigv

    def directmin_mz_only(self):
        self.file = open(self.txt, "a")
        print("Run SCF", datetime.now().strftime("%d/%m/%Y %H:%M:%S"), file=self.file)
        if self.use_constr:
            print("{}   {:>}   {:>}   {:>}".format("Iter:", "Error:", "Error constr:", "Energy:"), file=self.file)
        else:
            print("{}   {:>10s}   {:>10s}".format("Iter:", "Error:", "Energy:"), file=self.file)

        i = 0
        noa = self.noa
        self.forces = np.zeros_like(self.spins)
        if self.use_constr:
            var = self.nmthphi[:4,:].reshape(noa*4)
            var[2*noa:] = self.constr
        else:
            var = self.nmthphi[:2,:].reshape(noa*2)
        grad = np.zeros_like(var)

        if self.total_iters > 0:
            self.method = {'name': 'LSR1P', 'method': 'LSR1',
                           'memory': 20}

        sd_algo = search_direction(self.method)

        while True:
            n, m_x, m_y, m_z, eig, eigv = self.get_n_and_moms()
            m = m_x * self.spins[:,0] + m_y * self.spins[:,1] + m_z * self.spins[:,2]

            grad[:noa] = (var[:noa] - n) * self.mixing
            grad[noa:2*noa] = (var[noa:2*noa] - m) * self.mixing
            if self.use_constr:
                eth, ephi = tangent_vector_from_crts(self.spins)
                grad[2 * noa:3 * noa] = (m_x * eth[0] + m_y * eth[1] + m_z * eth[2]) * self.mixing
                grad[3 * noa:4 * noa] = (m_x * ephi[0] + m_y * ephi[1] + m_z * ephi[2]) * self.mixing
                self.forces[:, 0] = m_x * 0.5 * self.U * m
                self.forces[:, 1] = m_y * 0.5 * self.U * m
                self.forces[:, 2] = m_z * 0.5 * self.U * m
                dg = np.max(
                    np.linalg.norm(np.cross(self.spins, self.forces), axis=1))

            p_vec = sd_algo.update_data(var, grad)
            maxmove = np.max(np.absolute(p_vec))
            if maxmove > self.maxmove:
                p_vec *= self.maxmove / maxmove
            var += p_vec
            if self.use_constr:
                self.constr[:] = var[2 * noa:]

            energy = calc_energy(eig, n, m, self.U)
            i += 1
            dx = np.max(np.absolute(grad[:2*noa]))
            # if i % 100 == 0:
            # print(i, dx, energy)
            self.total_iters += 1
            if i == self.maxiter:
                break
            if dx < self.tol:
                if self.use_constr:
                    if dg < self.tol2:
                        print(i, dx, dg, energy, file=self.file, flush=True)
                        break
                else:
                    break
            if i % self.logevery == 0 or i == 1:
                if self.use_constr:
                    print(i, dx, dg, energy, file=self.file, flush=True)
                else:
                    print("{0:>5d}    {1:>1.2e}    {2:>.10f}".format(i, dx, energy), file=self.file, flush=True)

        # TODO: rewrite it in a more effecient way
        # print(i, dx, energy)
        if self.use_constr:
            print(i, dx, dg, energy, file=self.file, flush=True)
        else:
            print("{0:>5d}    {1:>1.2e}    {2:>.10f}".format(i, dx,
                                                             energy),
                  file=self.file, flush=True)
        if self.use_constr:
            self.forces[:, 0] = -(self.constr[:noa] * eth[0] + self.constr[noa:] * ephi[0]) * m
            self.forces[:, 1] = -(self.constr[:noa] * eth[1] + self.constr[noa:] * ephi[1]) * m
            self.forces[:, 2] = -(self.constr[:noa] * eth[2] + self.constr[noa:] * ephi[2]) * m
        else:
            self.forces[:,0] = m_x * 0.5 * self.U * m
            self.forces[:,1] = m_y * 0.5 * self.U * m
            self.forces[:,2] = m_z * 0.5 * self.U * m

        # m *= np.sign(m)
        self.energy = energy # + self.add_classical_corrections_energy()
        self.forces[:] = self.forces # + self.add_classical_corrections_forces()
        # print( np.allclose(self.constr[:], var[2*noa:]) )
        print("Finished!", file=self.file, flush=True)
        self.final_output(i, self.maxiter, n, m, energy,
                     np.cross(self.spins, self.forces))

    def initial_output(self):
        # dd/mm/YY H:M:S
        print(datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
              file=self.file)
        print('NCAA model', file=self.file)
        print('', file=self.file)
        print('Parameters:', file=self.file)
        print('E_0 = ', self.E0, ' (Gamma)', file=self.file)
        print('U = ', self.U, ' (Gamma)', file=self.file)
        print('V = ', self.V, ' (Gamma)', file=self.file)
        print('Z_ad = ', self.Z_ad, ' (Gamma)', file=self.file)
        print('K_ad = ', self.K_ad, ' (Gamma)', file=self.file)
        print('K2_ad = ', self.K2_ad, ' (Gamma)', file=self.file)
        print('Use constraints: ', self.use_constr, file=self.file)
        print('Atomic sphere radius: ', self.r_c, file=self.file)
        print('Number of atoms: ', self.noa, file=self.file)
        print('Maximum iterations: ', self.maxiter, file=self.file)
        print('Convergence cr. N, M: ', self.tol, file=self.file)
        print('Convergence cr. constr: ', self.tol2, file=self.file)

        print('', file=self.file)

        # self.file.close()

    def final_output(self, iters, maxiter, n, m, energy, torues):
        full = self.full_output

        a = self.get_noscf_angle()
        print('Max deviation angle of Exp[M] '
              'from spin dir.: ', a, file=self.file)

        print('SCF finished ', datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
              file=self.file)
        if iters >= maxiter:
            print('Maximum iterations reached. Did not convegred!', file=self.file)
        else:
            print('Converged after: ', iters, ' iterations',
                  file=self.file)
        print('Energy of the system: ', energy, ' (Gamma)',
              file=self.file)
        print('Maximum torque per atom ', np.max(np.linalg.norm(torues, axis=1)), ' (Gamma)',
              file=self.file)
        print('Total number of iterations:', self.total_iters,file=self.file)
        print('Average local magnetic moment:', np.average(np.absolute(m)),file=self.file)
        print('Average number of d-electrons:', np.average(np.absolute(n)),file=self.file)
        print('', file=self.file)

        if full:
            print('Atom, Occupation numbers, Magnetic moments:',
                  file=self.file)

            for i in range(len(n)):
                print(i, n[i], m[i], file=self.file)

            print('Spins:', file=self.file)
            for i in range(len(n)):
                print(i, self.spins[i, 0], self.spins[i, 1],
                      self.spins[i, 2], file=self.file)

            print('Atom, Torques (Gamma) x, y, z:', file=self.file)
            for i in range(len(n)):
                print(i, torues[i, 0], torues[i, 1],
                      torues[i, 2], file=self.file)
        self.file.close()

    def add_classical_corrections_energy(self):

        energy_dm = 0.0
        energy_a1 = 0.0
        energy_a2 = 0.0

        atoms, spins = self.atoms, self.spins

        if self.DM is not None:
            if type(self.DM) is float:
                D = self.DM
                bloch = 0
                perp_v = np.array([0.0, 0.0, 0.0])
            else:
                D, perp_v = \
                    self.DM['ampl'], np.asarray(self.DM['dir'])
                bloch = 1

            cell = atoms.get_cell()
            if not isinstance(cell,np.ndarray):
                cell = cell.array
            energy_dm = energy_module.dmenergy(D, spins,
                                            self.nbl1_ow,
                                            self.nbl2_ow,
                                            atoms.positions,
                                            self.offsets_ow,
                                            cell,
                                            perp_v, bloch
                                            )
        if self.K_ad is not None:
            K, e_c = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
            energy_a1 = energy_module.anenergy(K, e_c, spins)
        if self.K2_ad is not None:
            K, e_c = self.K2_ad['ampl'], np.asarray(self.K2_ad['dir'])
            energy_a2 = energy_module.anenergy(K, e_c, spins)

        return energy_dm + energy_a1 + energy_a2

    def add_classical_corrections_forces(self):
        atoms, spins = self.atoms, self.spins
        grad = np.zeros(shape=(spins.shape[0], 3))
        if self.DM is not None:
            if type(self.DM) is float:
                D = self.DM
                bloch = 0
                perp_v = np.array([0.0, 0.0, 0.0])
            else:
                D, perp_v = \
                    self.DM['ampl'], np.asarray(self.DM['dir'])
                bloch = 1
            cell = atoms.get_cell()
            if not isinstance(cell,np.ndarray):
                cell = cell.array

            gradient_module.dmgrad(D, spins, self.nbl1_bw,
                                   self.nbl2_bw, atoms.positions,
                                   self.offsets_bw, cell,
                                   grad, perp_v, bloch)
        if self.K_ad is not None:
            K, e_c = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
            gradient_module.angrad(K, e_c, spins, grad)
        if self.K2_ad is not None:
            K, e_c = self.K2_ad['ampl'], np.asarray(self.K2_ad['dir'])
            gradient_module.angrad(K, e_c, spins, grad)

        return -grad

    def get_noscf_angle(self):
        n, m_x, m_y, m_z, eig, eigv = self.get_n_and_moms()
        m = m_x * self.spins[:, 0] + \
            m_y * self.spins[:, 1] + \
            m_z * self.spins[:, 2]
        abs_m = np.sqrt(m_x**2 + m_y**2 + m_z**2)
        cos_a = m / abs_m
        a = np.max(np.arccos(np.clip(np.absolute(cos_a), -1, 1)))
        return a

    def get_density_of_states(self, omega, dos_type='++', gamma=1.0, anumbers=None):
        # omega = np.asarray(omega)
        noa = self.noa
        ham = np.zeros(shape=(2 * noa, 2 * noa), dtype=complex)
        Z, e_zdir = self.Z_ad['ampl'], np.asarray(self.Z_ad['dir'])
        K, e_kdir = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
        K2, e_k2dir = self.K2_ad['ampl'], np.asarray(
            self.K2_ad['dir'])

        ncaa_module.fillham(self.E0, self.U, self.V,
                            Z, e_zdir, K, e_kdir, K2, e_k2dir,
                            ham,
                            self.constr,
                            self.spins,
                            self.nmthphi,
                            self.nbl1_ow,
                            self.nbl2_ow)

        eig, eigv = np.linalg.eigh(ham)

        omega = np.tile(omega, (len(eig), 1)).T

        if dos_type == '++':
            q = np.abs(eigv[noa:, :]) ** 2.0
        elif dos_type == '--':
            q = np.abs(eigv[:noa, :]) ** 2.0
        else:
            raise NotImplementedError
        x = (gamma/(((omega - eig*gamma))**2 + gamma**2)).T

        if anumbers is None:
            rho_omega = np.sum(q @ x, axis=0)
        else:
            rho_omega = np.sum(q[anumbers,] @ x, axis=0)
        return rho_omega/np.pi

    def get_hamiltonian_matrix(self):
        noa = self.noa
        ham = np.zeros(shape=(2 * noa, 2 * noa), dtype=complex)
        Z, e_zdir = self.Z_ad['ampl'], np.asarray(self.Z_ad['dir'])
        K, e_kdir = self.K_ad['ampl'], np.asarray(self.K_ad['dir'])
        K2, e_k2dir = self.K2_ad['ampl'], np.asarray(self.K2_ad['dir'])

        ncaa_module.fillham(self.E0, self.U, self.V,
                            Z, e_zdir, K, e_kdir, K2, e_k2dir,
                            ham,
                            self.constr,
                            self.spins,
                            self.nmthphi,
                            self.nbl1_ow,
                            self.nbl2_ow)

        return ham


def tangent_vector(theta, phi):
    assert theta.shape == phi.shape

    eth = np.zeros(shape=(3, theta.shape[0]))
    ephi = np.zeros(shape=(3, theta.shape[0]))

    eth[0] = np.cos(theta) * np.cos(phi)
    eth[1] = np.cos(theta) * np.sin(phi)
    eth[2] = -np.sin(theta)

    ephi[0] = -np.sin(phi)
    ephi[1] = np.cos(phi)
    ephi[2] = 0.0

    return eth, ephi

def tangent_vector_from_crts(spins):

    eth = np.zeros(shape=(3, spins.shape[0]))
    ephi = np.zeros(shape=(3, spins.shape[0]))

    non_polse = (1.0 - np.absolute(spins[:,2])) > 1.0e-8
    cos_theta = spins[non_polse,2]
    sin_theta = np.sqrt(1.0 - cos_theta ** 2)
    cos_phi = spins[non_polse,0] / sin_theta
    sin_phi = spins[non_polse,1] / sin_theta

    eth[0][non_polse] = cos_theta * cos_phi
    eth[1][non_polse] = cos_theta * sin_phi
    eth[2][non_polse] = -sin_theta

    ephi[0][non_polse] = -sin_phi
    ephi[1][non_polse] = cos_phi

    eth[0][~non_polse] = 1.0
    eth[1][~non_polse] = 0.0
    eth[2][~non_polse] = 0.0
    ephi[0][~non_polse] = 0.0
    ephi[1][~non_polse] = 1.0

    return eth, ephi

def sph2cart(theta, phi):
    assert theta.shape == phi.shape

    vec = np.zeros(shape=(theta.shape[0], 3))

    vec[:,0] = np.sin(theta) * np.cos(phi)
    vec[:,1] = np.sin(theta) * np.sin(phi)
    vec[:,2] = np.cos(theta)


    return vec

def calc_energy(eig, n, m, U):
    return sum(eig * (np.pi / 2.0 - np.arctan(eig)) +
        0.5 * np.log(eig * eig + 1.0)) / np.pi - 0.25 * U * sum(
        n ** 2.0 - m ** 2.0)
