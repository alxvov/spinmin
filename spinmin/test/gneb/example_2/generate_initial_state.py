from ase.build import hcp0001, fcc111, bcc110
from spinmin.utilities import theta_function, plot_xy, from_mT_to_meV, collinear_state
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io

size = 200
a = 1.0

# Choose atom struct.
atoms = hcp0001('Fe', a=a, orthogonal=True, size=(size, size, 1))
# atoms.set_pbc((True, True, False))

# define interaction
interactions={'J': 29.0,
              'DM': {'ampl': 1.5,
                     'dir': [0.0, 0.0, 1.0]},
              'K_ad': {'ampl': 0.293,
                       'dir': [0.0, 0.0, 1.0]},
              'r_c': a/2+0.1}

# initial spin_configuration
# spins = collinear_state(len(atoms), along='z')
# flips the spins within a specific radius
spins = theta_function(atoms, 10*a)
tempspins = spins.copy()
# initialise the hamiltonian
ham = SpinHamiltonian(atoms, spins, interactions)

# initialises the Unitary Optimisation and runs the calculation for the minimum
opt = UnitaryOptimisation(ham)
opt.run()

# plots the initial spin state and saves the Atom class for the MEP calculation
plot_xy(atoms, spins, 'init_state_skyrm')


io.write('init_state_skyrm.traj', atoms)