from ase.build import hcp0001, fcc111, fcc110, bcc110
from spinmin.utilities import collinear_state, plot_xy, from_mT_to_meV
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io

a = 1.0 # /0.866025
# Choose atom struct.
atoms = bcc110('Fe', a=a, orthogonal=True, size=(30, 10, 1))
# set the periodic boundary condition off
atoms.set_pbc(False)

# define interaction
interactions = {'J': 25.6,
                'K_ad': {'ampl': 1.2,
                         'dir': [0.0, 1.0, 0.0]},
                'K2_ad': {'ampl': -0.5,
                          'dir': [0.0, 0.0, 1.0]}
                }

# initial spin_configuration to -y
spins = -collinear_state(len(atoms), along='y')

# initialise the hamiltonian
ham = SpinHamiltonian(atoms, spins, interactions)

# initialises the Unitary Optimisation and runs the calculation for the minimum
opt = UnitaryOptimisation(ham, searchdir_algo='LBFGS2', linesearch_algo='CutOff')
opt.run()

# plots the final spin state and saves the Atom class for the MEP calculation
plot_xy(atoms, spins, 'final_state')
io.write('final_state.traj', atoms)
