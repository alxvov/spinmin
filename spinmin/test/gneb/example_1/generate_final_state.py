from ase.build import hcp0001, fcc111, fcc110, bcc110
from spinmin.utilities import collinear_state, plot_xy, from_mT_to_meV
from spinmin.hamiltonian import SpinHamiltonian
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io

size = 200
a = 1.0

# Choose atom struct.
atoms = fcc111('Fe', a=a, orthogonal=True, size=(size, size, 1))
# atoms.set_pbc((True, True, False))

# define interaction
interactions = {'J': 3.68*2,
                'DM': {'ampl': 1.39*2,
                       'dir': [0.0, 0.0, 1.0]},
                'Z_ad': {'ampl': from_mT_to_meV(3, 4000),
                         'dir': [0.0, 0.0, 1.0]},
                'K_ad': {'ampl': 0.7,
                         'dir': [0.0, 0.0, 1.0]}
                }

# initial spin_configuration
spins = collinear_state(len(atoms), along='z')

# initialise the hamiltonian
ham = SpinHamiltonian(atoms, spins, interactions)

# initialises the Unitary Optimisation and runs the calculation for the minimum
opt = UnitaryOptimisation(ham)
opt.run()

# plots the final spin state and saves the Atom class for the MEP calculation
plot_xy(atoms, spins, 'final_state_skyrm')
io.write('final_state_skyrm.traj', atoms)
