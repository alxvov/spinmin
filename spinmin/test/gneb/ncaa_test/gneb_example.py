from spinmin.gneb.gneb import GNEB
from spinmin.ncaa.hamiltonian  import NCAAHamiltonian
from ase import io
import numpy as np

# first minimum
atoms_i = io.read('init_state.traj')
spins_i = atoms_i.get_initial_magnetic_moments()
ham_i = NCAAHamiltonian(atoms_i, spins_i,
                        interactions={'E0': -12.0,
                                      'U': 13.0,
                                      'V': 0.5},
                        convergence=1.0e-7,
                        maxiter=1000,
                        txt='output_0.txt',
                        method='SD')

# second minimum
atoms_f = io.read('final_state.traj')
spins_f = atoms_f.get_initial_magnetic_moments()
ham_f = NCAAHamiltonian(atoms_f, spins_f,
                        interactions={'E0': -12.0,
                                      'U': 13.0,
                                      'V': 0.5},
                        convergence=1.0e-7,
                        maxiter=1000,
                        txt='output_7.txt',
                        method='SD')

gneb = GNEB(spring=[2.0e-3, 1.0e-3])
# creates a chain of images with number of images (noi) and linearly interpolates the spins
noi = 8
hamlist = [ham_i]
for i in range(noi-2):
    h = NCAAHamiltonian(atoms_i, spins_i,
                 interactions={'E0': -12.0,
                               'U': 13.0,
                                'V': 0.5},
                  convergence=1.0e-7,
                  mixing=1.0,
                  maxiter=1000,
                  txt='output_' + str(i + 1) + '.txt',
                  method='SD',
                  use_constr=False)
    hamlist.append(h)
hamlist.append(ham_f)
gneb.interpolate_chain(ham_i, ham_f, noi=noi, hamlist=hamlist)
gneb.plot_interpolation(name='InitialInterpolation', factor=1000)
gneb.plot_chain('Initimage')

from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
opt = UnitaryOptimisation(gneb, convergence=1.0e-8, max_iter=20)

opt.run()

gneb.plot_chain(name='FinalFig')
gneb.plot_interpolation('FinalInterpolation', factor=1000)

print(np.allclose(gneb.e_total,
                  [-1309.5529577, -1309.5524959, -1309.5518134, -1309.5534003,
                   -1309.56743465, -1309.5755652, -1309.57878828, -1309.57964935]
                  ))
