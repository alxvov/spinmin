from ase.build import bcc100, hcp0001
from spinmin.effective_ham.cross_dot import SpinHamiltonian
import numpy as np
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io
import matplotlib.pyplot as plt
import time
from spinmin.utilities import collinear_state, skyrm_function, plot_xy
from ase.visualize import view
from scipy.interpolate import interp1d
a = 10.0
N = 50
atoms = hcp0001('Fe', a=a, orthogonal=False, size=(N, N, 1))

nnn = 3

spins = skyrm_function(atoms, 10.0*a)
# spins = collinear_state(len(atoms))
atoms.set_initial_magnetic_moments(spins)

J = [1.0]*3
expnt = [4.0]*3
T = [10.0]*3

ham = SpinHamiltonian(atoms, spins,
                      interactions={'J': J,
                      'T': T, 'eps': expnt})



def d2_theta_ij_2(ham, i, j):

    """
    d^2 E / dtheta_i dtheta_j

    state must be collinear with
    all the magnetic moments point in z direction

    :param i:
    :param j:
    :return:
    """

    eps = 1.0e-4
    coef = [1.0, -1.0,]
    displ = [1.0, -1.0]
    second_der = 0.0

    theta_i = 0.0
    theta_j = 0.0

    for k in range(2):

        theta_j += displ[k] * eps

        ham.spins[j] = \
            np.asarray([np.sin(theta_j), 0.0, np.cos(theta_j)])

        e, g = ham.get_energy_and_gradients()
        fi = g[i][0]
        second_der += fi * coef[k]

        theta_j = 0.0
        ham.spins[j] = \
            np.asarray([np.sin(theta_j), 0.0, np.cos(theta_j)])

    return second_der / (2.0 * eps)


p0 = atoms.positions[0]
dist = np.asarray([np.linalg.norm(p0 - p) for p in atoms.positions])
inds = np.argsort(dist)

new_dist = [0.0]
new_inds = [0]
for j in inds:
    if (dist[j] - new_dist[-1]) < 1.0e-2:
        continue
    else:
        new_dist.append(dist[j])
        new_inds.append(j)

Z=np.zeros(shape=nnn)

center = len(atoms)//2 + N//2
p0 = atoms.positions[center]
for j in range(len(atoms)):
    if j == center:
        continue
    p = atoms.positions[j]
    i = 0
    r_ij = np.linalg.norm(p0 - p)
    for rad in dist[1:nnn+1]:
        if np.abs(r_ij - rad) < 0.01:
            Z[i] += 1
            break
        i += 1
print('NN =',Z)



my_ind = new_inds[1:nnn+1]
# print(dist[my_ind])
# print(my_ind)


D_ij = []
for j in my_ind:
    D_ij.append(d2_theta_ij_2(ham, 0, j))

print(D_ij)
