from ase.build import bcc100, hcp0001
from spinmin.hamiltonian import SpinHamiltonian
import numpy as np
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io
import matplotlib.pyplot as plt
import time
from spinmin.utilities import collinear_state, skyrm_function, plot_xy
from ase.visualize import view
from scipy.interpolate import interp1d
a = 1.0
N = 36
atoms = hcp0001('Fe', a=a, orthogonal=False, size=(N, N, 1))
spins = skyrm_function(atoms, 15.0*a)
atoms.set_initial_magnetic_moments(spins)

# LKAG
Js = [5.536086992003163,
      -1.4015137884323228,
      -0.36054595272654055,
      0.06210704103925329,
      0.022230489875200665,
      -0.00017424244886522468,
      -0.0019278176404764298,
      -0.0013801201075828403]


ham = SpinHamiltonian(atoms, spins,
                      interactions={'J': Js,
                                    })

opt = UnitaryOptimisation(ham,
                          convergence=1.0e-5/Js[0])
opt.run()

e1 = ham.get_energy_and_gradients()[0]
atoms.set_initial_magnetic_moments(ham.spins)
io.write('init.traj', atoms)
plot_xy(atoms, spins, 'Final36x36')

spins = collinear_state(len(atoms))
ham = SpinHamiltonian(atoms, spins,
                      interactions={'J': Js,
                                    })
e2 = ham.get_energy_and_gradients()[0]
print(e2-e1) # -22.175122435757658
atoms.set_initial_magnetic_moments(ham.spins)
io.write('ferromagn.traj', atoms)


