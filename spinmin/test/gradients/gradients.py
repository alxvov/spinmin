from ase.build import bcc100, hcp0001
from spinmin.hamiltonian import SpinHamiltonian
import numpy as np
from spinmin.minimise.unitary_minimisation import UnitaryOptimisation
from ase import io
import matplotlib.pyplot as plt
import time
from spinmin.utilities import collinear_state, skyrm_function, plot_xy
from ase.visualize import view
from scipy.interpolate import interp1d
a = 1.0
N = 12
atoms = hcp0001('Fe', a=a, orthogonal=False, size=(N, N, 1))

spins = skyrm_function(atoms, 10.0*a) # collinear_state(len(atoms))
atoms.set_initial_magnetic_moments(spins)

Js = [8.007470190980733,
-1.3867634349311901,
-0.36054595272654055,
0.06210704103925329,
0.022230489875200665,
-0.00017424244886522468,
-0.0019278176404764298,
-0.0013801201075828403
]


I24 = -0.43375010785373647
I34 = -0.41203351410601385
I44 = 0.01475035352571652
K44 = 0.134
B3 = 0.987
T1 = -0.765

ham = SpinHamiltonian(atoms, spins,
                      interactions={'J': Js,
                                    'I24': I24,
                                    'I34': I34,
                                    'I44': I44,
                                    'K44': K44,
                                    'B3': B3,
                                    'T1': T1,
                                    'DM': 0.934,
                                    'Z_ad': {'ampl': 0.012,
                                             'dir': [1.0, 1.0, 1.0]
                                            },
                                    'K1_ad': {'ampl': -0.02,
                                             'dir': [1.0, 0.1, 1.0]
                                             },
                                    'K2_ad': {'ampl': 0.01,
                                             'dir': [1.0, 1.0, 1.3]
                                             }
                                    }
                      )
opt = UnitaryOptimisation(ham,
                          convergence=1.0e-7)
g1, g2 = opt.calc_numerical()

print(np.allclose(g1, g2))
print(np.max(np.abs(g1-g2)))
